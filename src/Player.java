import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Player {

    private int minBuff;
    private int maxBuff;
    private double alfa;
    private final int FRAGMENTLENGTH = 4;

    private boolean paused;
    private int elapsedTime;
    private int playedTime;
    private int videoLength;

    private double lastEstimate;

    private boolean pausDownload;
    private int buffer;
    private Queue<Integer> bufferQueue;
    private int nowPlaying;


    private int fragmentSize;
    private double downloaded;
    private int downloadTime;
    private boolean downloading;
    private int lastFragmentIndex;
    private int requestedFragment;
    private ArrayList<Integer> bitrates;

    public Player(int videoLength, int minBuff, int maxBuff, double alfa) {
        this.videoLength = videoLength;
        this.minBuff = minBuff;
        this.maxBuff = maxBuff;
        this.alfa = alfa;

        this.paused = true;
        this.elapsedTime = 0;
        this.playedTime = 0;

        this.lastEstimate = 0;

        this.pausDownload = false;
        this.buffer= 0;
        this.bufferQueue  = new LinkedList<>();
        this.nowPlaying = 0;

        this.fragmentSize = 0;
        this.downloaded = 0;
        this.downloadTime = 0;
        this.downloading = false;
        this.lastFragmentIndex = 0;
        this.requestedFragment = -1;
        this.bitrates = new ArrayList<>();
        Integer[] bitrates = new Integer[] {250, 500, 850, 1300};
        this.bitrates.addAll(Arrays.asList(bitrates));
    }

    public boolean iterate(double bandwidth) {
        this.elapsedTime += 1;

        playVideo();

        if (this.buffer < this.maxBuff && !this.downloading && !this.pausDownload &&(this.playedTime + this.buffer) != this.videoLength) {
            startDownload();
        }
        else {
            this.requestedFragment = -1;
        }
        if (this.downloading) {
            downloadFragment(bandwidth);
        }

        printStuff(bandwidth);

        return this.playedTime == this.videoLength;
    }

    public void playVideo() {
        if (this.paused && this.buffer >= minBuff) {
            this.buffer--;
            playedTime++;
            this.paused = false;

            this.nowPlaying = this.bufferQueue.poll();
        }
        else if (!this.paused && this.buffer > 0) {
            this.buffer--;
            playedTime++;

            this.nowPlaying = this.bufferQueue.poll();

        }
        else {
            this.paused = true;
        }

        if (this.buffer < this.minBuff) {
            this.pausDownload = false;
        }
    }

    public void startDownload() {
        double estimatedBandwidth = getEstimateOpt1();
        int fragmentIndex = 0;

        for (int i = 0; i < this.bitrates.size(); i++) {
            if (estimatedBandwidth > bitrates.get(i)) {
                fragmentIndex = i;
            }
        }

        if (fragmentIndex < this.lastFragmentIndex - 2) {
            fragmentIndex = this.lastFragmentIndex - 2;
        }
        if (fragmentIndex > this.lastFragmentIndex + 1) {
            fragmentIndex = this.lastFragmentIndex + 1;
        }
        int chosenBitrate = bitrates.get(fragmentIndex);

        this.lastFragmentIndex = fragmentIndex;
        this.requestedFragment = fragmentIndex;
        this.fragmentSize = chosenBitrate * FRAGMENTLENGTH;
        this.downloadTime = 0;
        this.downloading = true;
    }

    public void downloadFragment(double bandwidth) {
        this.downloaded += bandwidth;
        this.downloadTime += 1;
        if (this.downloaded >= fragmentSize) {
            this.downloaded = 0;
            this.downloading = false;
            this.buffer += FRAGMENTLENGTH;
            this.bufferQueue.offer(this.lastFragmentIndex);
            this.bufferQueue.offer(this.lastFragmentIndex);
            this.bufferQueue.offer(this.lastFragmentIndex);
            this.bufferQueue.offer(this.lastFragmentIndex);

            if (this.buffer > this.maxBuff) {
                this.pausDownload = true;
            }
        }
    }

    public double getEstimateOpt2() {
        double estimate = (1 - alfa) * this.lastEstimate;
        if (this.downloadTime != 0) {
            estimate += alfa * ((double) this.fragmentSize / this.downloadTime);
        }

        this.lastEstimate = estimate;
        return estimate;
    }

    public double getEstimateOpt1() {
        double estimate = this.lastEstimate;
        if (this.downloadTime != 0) {
            estimate = (double) this.fragmentSize / this.downloadTime;
        }

        this.lastEstimate = estimate;
        return estimate;
    }

    public String getBuffer() {
        return Integer.toString(this.buffer);
    }

    public String getNowPlayingIndex() {
        return Integer.toString(this.nowPlaying);
    }

    public String getQualityIndex() {
        return Integer.toString(this.requestedFragment);
    }

    public void printStuff(double bandwidth) {
        System.out.println("Elapsed Time: " + this.elapsedTime);
        System.out.println("Buffer: " + this.buffer);
        System.out.println("Bandwidth: " + bandwidth);
        System.out.println("Fragment: " + this.fragmentSize / 4);
        System.out.println("Downloaded: " + (int)((downloaded / fragmentSize) * 100) + " %");
        System.out.println();
    }
}
