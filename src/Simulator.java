import java.io.*;
import java.util.ArrayList;

public class Simulator {
    public static void main(String[] args) {
        try {
            ArrayList<Double> bandwidths = getBandwidthList("log.txt");
            Player player = new Player(120, 4, 6, 0);

            ArrayList<String> input1 = new ArrayList<>();
            ArrayList<String> input2 = new ArrayList<>();
            ArrayList<String> input3 = new ArrayList<>();

            for (Double bandwidth : bandwidths) {
                boolean done = player.iterate(bandwidth);

                input1.add(player.getBuffer());
                input2.add(player.getNowPlayingIndex());
                input3.add(player.getQualityIndex());

                if (done) {
                    break;
                }
            }

            writeToFiles(input1, input2, input3);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static ArrayList<Double> getBandwidthList(String filePath) throws IOException {
        File logFile = new File(filePath);
        BufferedReader reader = new BufferedReader(new FileReader(logFile));

        ArrayList<Double> bandwidths = new ArrayList<Double>();
        String row;

        while ((row = reader.readLine()) != null) {
            String[] rowData = row.split(" ");
            double bytes = Double.parseDouble(rowData[4]);
            double millisec = Double.parseDouble(rowData[5]);
            double bandwidth = (bytes / millisec) * 8; //kBit/s
            bandwidths.add(bandwidth);
        }

        return bandwidths;
    }

    private static void writeToFiles(ArrayList<String> input1, ArrayList<String> input2, ArrayList<String> input3) throws IOException {
        FileWriter writer1 = new FileWriter("input1.txt");
        FileWriter writer2 = new FileWriter("input2.txt");
        FileWriter writer3 = new FileWriter("input3.txt");

        for (int i = 0; i < input1.size(); i++) {
            writer1.write(i + " " + input1.get(i) + "\n");
            writer2.write(i + " " + input2.get(i) + "\n");
            if (Integer.parseInt(input3.get(i)) != -1) {
                writer3.write(i + " " + input3.get(i) + "\n");
            }
        }

        writer1.close();
        writer2.close();
        writer3.close();
    }
}
